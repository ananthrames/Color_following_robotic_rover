
#include <AFMotor.h> //import your motor shield library

AF_DCMotor motor1(1,MOTOR12_64KHZ); // set up motors.
AF_DCMotor motor2(2,MOTOR12_64KHZ);





String bluetoothRead, Str_x,Str_y,Str_p;
int x ;
int y ;
int points;
int length;




void setup() {
  Serial.begin(9600);       
  
  
//optional..  
// motor1.setSpeed(105); //set the speed of the motors, between 0-255
//motor2.setSpeed (105);  
  

}

void loop() {
 int i=0;
  char commandbuffer[200];


  if(Serial.available()){
    
     delay(10);

     while( Serial.available() && i< 199) {
        commandbuffer[i++] = Serial.read();
  
      
     }
     commandbuffer[i++]='\0';
     bluetoothRead = (char*)commandbuffer;
     length = bluetoothRead.length();
  
  
     
     if(bluetoothRead.substring(0, 1).equals("x")){
       
       int i=1;
       while(bluetoothRead.substring(i, i+1) != ("y")){
       i++;
       }
       
       Str_x = bluetoothRead.substring(1, i);
       x = Str_x.toInt();
       
 
     
       Str_y = bluetoothRead.substring(i+1, length -1);
       y = Str_y.toInt();
       
        Str_p = bluetoothRead.substring(length - 1, length);
        points = Str_p.toInt();

       i = 1;
       
      
      Stop();
 
 if(x < 40){
   Left();
  
  }
  if(x > 140){
    Right();
    
  } 
  if(x < 140 && x > 40){
    if(points == 1){
   Forward(); 
    }
    if(points == 0){
    Stop();
    }
    if(points == 2){
     Back(); 
    }
  }

     

     }
}
}





void Left(){
  motor1.setSpeed(105); //Define maximum velocity
  motor1.run(FORWARD); //rotate the motor clockwise
  motor2.setSpeed(0);
  motor2.run(RELEASE); //turn motor2 off
  
}


void Right(){
  
  motor1.setSpeed(0);
  motor1.run(RELEASE); //turn motor1 off
  motor2.setSpeed(105); //Define maximum velocity
  motor2.run(FORWARD); //rotate the motor clockwise
  delay(10);
  Stop();
  
}


void Forward(){
  motor1.setSpeed(105); //Define maximum velocity
  motor1.run(FORWARD); //rotate the motor clockwise
  motor2.setSpeed(105); //Define maximum velocity
  motor2.run(FORWARD); //rotate the motor clockwise
  delay(10);
  Stop();
}


void Back(){
  motor1.setSpeed(105); 
  motor1.run(BACKWARD); //rotate the motor counterclockwise
  motor2.setSpeed(105); 
  motor2.run(BACKWARD); //rotate the motor counterclockwise
  delay(10);
  Stop();
}


void Stop(){ 
  motor1.setSpeed(0);
  motor2.run(RELEASE); //turn motor1 off
  motor2.setSpeed(0);
  motor2.run(RELEASE); //turn motor2 off
}

